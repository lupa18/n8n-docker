FROM node:16
MAINTAINER Lu Pa <admin@tedic.org>

ENV DEBIAN_FRONTEND noninteractive
ENV CODE /usr/src
ENV CODE_URL https://github.com/n8n-io/n8n.git

RUN apt-get update \
        && apt-get install -y \
           curl \
		       netcat \
        && apt-get clean

WORKDIR $CODE

# Copy code to conatiner volume
RUN git clone $CODE_URL n8n

# Copio los entrypoint
COPY docker-entrypoint.sh /

CMD ["/usr/src/n8n/node_modules/n8n/bin/n8n"]
