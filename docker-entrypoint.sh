#!/bin/bash

# Wait for DB
until nc -z -v -w30 postgres.n8n-docker_default 5432
do
  echo "Waiting for database connection..."
  # wait for 5 seconds before check again
  sleep 5
done
echo "Database ready..."

cd /usr/src/n8n

if [ ! -d "node_modules/n8n" ]; then
  echo "Ejecutando npm install:"
  /usr/local/bin/npm install n8n
fi

exec "$@"
